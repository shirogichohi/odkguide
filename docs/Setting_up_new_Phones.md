# **SETTING UP NEW PHONES FOR ODK**

### **Steps**

1. Connect to Wifi. If setting up multiple phones simultaneously, keep the maximum of connections at 7 for an efficient installation.
2. Log in to FAO Somalia ODK Google Account.
3. First Start Configuration: Leave all default configs but **disable Screen Lock** when you get to that part.
4. When asked to restore apps, select one of the most recently used devices on the list and restore only ODK