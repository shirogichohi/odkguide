To obtain the form please follow the below steps:
!!! note
    Make sure you have ODK installed.
    


**Steps:**

1. **Open ODK Collect App**

![](images/odk_collect.jpg)

**2. Navigate to**  **Settings -\&gt; Admin Settings ** - **\&gt; Import/Export settings**

![](images/odk_admin.jpg)



**6.** **On the ODK Home Menu, click on &quot;** **Get Blank Forms&quot;**


![](images/odk_get_blank_form.jpg)

**7.** **You should be able to select the form , Example :**  &quot; **FAOSO Beneficiary Registration Form&quot;**

![](images/odk_form_selected.jpg)

**8.	Click “Get Selected”. Ensure you get a Success message in the pop up.**

![](images/odk_success.jpg)
